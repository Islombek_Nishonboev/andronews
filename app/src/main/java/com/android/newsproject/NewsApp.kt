package com.android.newsproject

import android.app.Application
import com.android.newsproject.di.AppComponent

class NewsApp : Application() {

    lateinit var sApp: NewsApp
    lateinit var sAppComponent: AppComponent


    fun getsApp(): NewsApp {
        return sApp
    }

    fun setsApp(sApp: NewsApp) {
        this.sApp = sApp
    }

    fun getsAppComponent(): AppComponent{
        return sAppComponent
    }



    override fun onCreate() {
        super.onCreate()
        sApp = this
        sAppComponent = DaggerAppComponent.builder().app(this).build()
        sAppComponent.inject(this)
    }
}