package com.android.newsproject.di

import com.android.newsproject.NewsApp
import com.android.newsproject.di.module.AppModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

/*
* @author Islombek Nishonboev
* @since 2021
*/

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(app: NewsApp)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun app(app: NewsApp): Builder
        fun build(): AppComponent
    }
}