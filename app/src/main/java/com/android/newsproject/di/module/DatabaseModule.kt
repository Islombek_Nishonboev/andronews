package com.android.newsproject.di.module

import android.content.Context
import androidx.room.Room.databaseBuilder
import com.android.newsproject.data.db.AppDatabase
import com.android.newsproject.data.db.DbService
import com.android.newsproject.data.db.DbServiceImpl
import com.android.newsproject.di.AppContext
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabaseService(database: AppDatabase): DbService {
        return DbServiceImpl(database)
    }

    @Provides
    fun provideDatabase(@AppContext context: Context): AppDatabase {
        return databaseBuilder(context, AppDatabase::class.java,"newsapp.db")
                .allowMainThreadQueries()
                .build()
    }
}