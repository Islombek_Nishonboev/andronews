package com.android.newsproject.di.module

import android.content.Context
import com.android.newsproject.data.config.ConfigService
import com.android.newsproject.data.config.ConfigServiceImpl
import com.android.newsproject.di.AppContext
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ConfigModule {

    @Provides
    @Singleton
    fun providePrefsService(@AppContext context: Context) : ConfigService{
        return ConfigServiceImpl(context,"configurations")
    }
}