package com.android.newsproject.di

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import javax.inject.Qualifier

/*
* @author Islombek Nishonboev
* @since 2021
*/


@Qualifier
@Retention(RetentionPolicy.RUNTIME)
annotation class AppContext