package com.android.newsproject.di.module

import android.app.Application
import android.content.Context
import com.android.newsproject.NewsApp
import com.android.newsproject.di.AppContext
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


/*
* @author Islombek Nishonboev
* @since 2021
*/


@Singleton
@Module(includes = [NetworkModule::class, ConfigModule::class, DatabaseModule::class])
 abstract class AppModule {

    /**
     * Provides the application context.
     */

    @Provides
    @AppContext
    fun provideAppContext(application: Application): Context? {
        return application.applicationContext
    }

    @Binds
    @Singleton
    abstract fun application(app: NewsApp): Context?
}