package com.android.newsproject.di.module

import android.content.Context
import android.os.Build
import com.android.newsproject.BuildConfig
import com.android.newsproject.data.network.Api
import com.android.newsproject.data.network.ApiService
import com.android.newsproject.data.network.ApiServiceImpl
import com.android.newsproject.di.AppContext
import com.readystatesoftware.chuck.ChuckInterceptor
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.create
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideNetworkService(@AppContext context: Context, api: Api): ApiService {
        return ApiServiceImpl(context, api)
    }

    @Provides
    fun provideAPI(moshi: Moshi, okHttpClient: OkHttpClient): Api {
        val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
        return retrofit.create(Api::class.java)

    }

    @Provides
    fun provideOkHttpClient(@AppContext context: Context): OkHttpClient {
        val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        val okHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) okHttpClientBuilder.addInterceptor(ChuckInterceptor(context))
        okHttpClientBuilder.addInterceptor(interceptor)

        return okHttpClientBuilder
                .connectTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .cache(null)
                .build()
    }

    @Provides
    fun provideMoshi(): Moshi {
        return Moshi.Builder().build()
    }
}