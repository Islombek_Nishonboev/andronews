package com.android.newsproject.ui.home.search;

import android.content.Context;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.newsproject.R;
import com.android.newsproject.ui.MainActivity;
import com.android.newsproject.ui.callback.MainClickListener;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class SearchFragment extends Fragment {

    MainClickListener mListener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mListener = (MainActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }


    @OnClick(R.id.btn_back_search)
    void onClickSearch(View view) {
        switch (view.getId()) {
            case R.id.btn_back_search: {
                mListener.onHomeClicked();
                break;
            }
        }
    }
}