package com.android.newsproject.ui.home;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.android.newsproject.ui.home.homeChild.HomeChildFragment;

import java.util.List;

public class HomePagerAdapter extends FragmentPagerAdapter {

    private List<String> mPageTitles;

    public void setPageTitles(List<String> pageTitles) {
        mPageTitles = pageTitles;
    }

    public HomePagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return new HomeChildFragment();
    }

    @Override
    public int getCount() {
        return mPageTitles.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mPageTitles.get(position);
    }
}
