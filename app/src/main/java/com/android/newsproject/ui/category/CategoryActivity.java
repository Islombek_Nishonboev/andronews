package com.android.newsproject.ui.category;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.android.newsproject.R;
import com.android.newsproject.data.model.CategoryItem;
import com.android.newsproject.ui.MainActivity;
import com.android.newsproject.ui.callback.MainClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryActivity extends AppCompatActivity {

    @BindView(R.id.recyclerview_category)
    RecyclerView mRecycler;

    CategoryRecyclerAdapter mRecyclerAdapter;

    List<CategoryItem> mList;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_category);
        ButterKnife.bind(this);




        //RecyclerView
        mRecyclerAdapter = new CategoryRecyclerAdapter();
        mRecycler.setLayoutManager(new LinearLayoutManager(this));
        mRecycler.setAdapter(mRecyclerAdapter);
        mList = new ArrayList<>();
        addToCategory();
    }

    void addToCategory() {

        mList.add(new CategoryItem(R.drawable.img_category, "World"));
        mList.add(new CategoryItem(R.drawable.img_category, "Technology"));
        mList.add(new CategoryItem(R.drawable.img_category, "Science"));
        mList.add(new CategoryItem(R.drawable.img_category, "Sport"));

        mRecyclerAdapter.setItemList(mList);
        mRecyclerAdapter.notifyDataSetChanged();

    }

    @OnClick(R.id.btn_back_category)
    void onClick(){
        onBackPressed();
        }
    }