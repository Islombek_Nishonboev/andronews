package com.android.newsproject.ui.account;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.newsproject.R;
import com.android.newsproject.data.model.LeftNavigationItem;
import com.android.newsproject.ui.account.language.LanguageFragment;
import com.android.newsproject.ui.account.more.MoreFragment;
import com.android.newsproject.ui.account.notification.NotificationFragment;
import com.android.newsproject.ui.account.profile.ProfileFragment;
import com.android.newsproject.ui.account.settings.SettingsFragment;
import com.android.newsproject.ui.splash.FirstSplashFragment;
import com.android.newsproject.ui.splash.SecondSplashFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AccountFragment extends Fragment implements AccountRecyclerAdapter.onNavigationClicked {

    @BindView(R.id.recyclerview_account)
    RecyclerView mRecycler;
    @BindView(R.id.text_toolbar_account)
    TextView textToolbar;

    AccountRecyclerAdapter mAdapter;

    List<LeftNavigationItem> mList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        //RecyclerView
        mAdapter = new AccountRecyclerAdapter();
        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecycler.setAdapter(mAdapter);
        mAdapter.setListener(this);
        //

        mList = new ArrayList<>();
        addToNavigation();

        //Navigation
        onPageChanged(new ProfileFragment());


    }
    private void addToNavigation(){
        mList.add(new LeftNavigationItem(R.drawable.ic_person_sign,1));
        mList.add(new LeftNavigationItem(R.drawable.ic_settings,2));
        mList.add(new LeftNavigationItem(R.drawable.ic_bell,3));
        mList.add(new LeftNavigationItem(R.drawable.ic_world,4));
        mList.add(new LeftNavigationItem(R.drawable.ic_keypad,5));

        mList.get(0).setSelected(true);

        mAdapter.setItemList(mList);
        mAdapter.notifyDataSetChanged();
    }



    void onPageChanged(Fragment fragment){
                 getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.container_account, fragment)
                .commit();
    }

    @Override
    public void onNavigationClicked(LeftNavigationItem itemList) {
        switch (itemList.getOrderNumber()){
            case 1:{
                onPageChanged(new ProfileFragment());
                textToolbar.setText("Account settings");
                break;
            }
             case 2:{
                 onPageChanged(new SettingsFragment());
                 textToolbar.setText("App settings");
                break;
            }
             case 3:{
                onPageChanged(new NotificationFragment());
                 textToolbar.setText("App notification");
                break;
            }
             case 4:{
                onPageChanged(new LanguageFragment());
                 textToolbar.setText("App language");
                break;
            }
             case 5:{
                onPageChanged(new MoreFragment());
                 textToolbar.setText("More");
                break;
            }
        }
    }
}