package com.android.newsproject.ui.home;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.newsproject.R;
import com.android.newsproject.ui.MainActivity;
import com.android.newsproject.ui.callback.MainClickListener;
import com.android.newsproject.ui.home.search.SearchFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


@RequiresApi(api = Build.VERSION_CODES.R)
public class HomeFragment extends Fragment {

    @BindView(R.id.tablayout_home)
    TabLayout mTab;
    @BindView(R.id.view_pager_home)
    ViewPager mPager;

    HomePagerAdapter mPagerAdapter;

    private MainClickListener mListener;

    List<String> homePageTitles = List.of("Hot", "World", "Business", "Technology");

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        mListener = (MainActivity) context;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        mPagerAdapter = new HomePagerAdapter(getChildFragmentManager());
        mPagerAdapter.setPageTitles(homePageTitles);

        mPager.setAdapter(mPagerAdapter);

        mTab.setupWithViewPager(mPager);
    }

    @OnClick({R.id.btn_search, R.id.btn_menu})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_search: {
                mListener.onSearchClicked();
                break;
            }
            case R.id.btn_menu: {
                mListener.onNavBarClicked();
                break;
            }
        }

    }
}