package com.android.newsproject.ui.home.homeChild;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.newsproject.R;
import com.android.newsproject.data.constants.Constants;
import com.android.newsproject.data.model.NewsItemList;
import com.android.newsproject.ui.MainActivity;
import com.android.newsproject.ui.callback.MainClickListener;
import com.android.newsproject.ui.home.newsInfo.NewsInfoActivity;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeChildFragment extends Fragment implements HomeRecyclerAdapter.OnNewsItemClickListener {

    @BindView(R.id.recyclerview_home)
    RecyclerView mRecycler;

    @BindView(R.id.category_name_first)
    TextView mCategoryFirst;
    @BindView(R.id.title_news_first)
    TextView mTitleFirst;
    @BindView(R.id.news_time_first)
    TextView mTimeFirst;
    @BindView(R.id.comments_number_first)
    TextView mCommentsNumber;
    @BindView(R.id.seen_number_first)
    TextView mSeenNumber;
    @BindView(R.id.img_main_news)
    ImageView mMainImg;
    @BindView(R.id.main_news_container)
    CardView mContainer;

    HomeRecyclerAdapter mAdapter;


    List<NewsItemList> mList;

    //Comments
//    List<CommentItem> mCommentList;
//    CommentsRecyclerAdapter mCommentAdapter;
    //
    MainClickListener mListener;


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mListener = ((MainActivity) context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home_child, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        mList = new ArrayList<>();
//        mCommentList = new ArrayList<>();

        mAdapter = new HomeRecyclerAdapter();
        mAdapter.setListener(this);
        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecycler.setAdapter(mAdapter);

        //
        mContainer.setOnClickListener(view1 -> startNewsInfo(mList.get(0)));

        //Comments
//        mCommentAdapter = new CommentsRecyclerAdapter();
//        addComment();
        addToNews();



    }

//    void addComment() {
//        mCommentList.add(new CommentItem(R.drawable.img_travel_interests, "John Traveler",
//                "Yes I think so", "1 hour"));
//        mCommentAdapter.setCommentList(mCommentList);
//        mCommentAdapter.notifyDataSetChanged();
//    }

    void addToNews() {
        mList.add(new NewsItemList("World", "Corona Virus",
                "Corona Virus is Killing people", "2 hours", "32",
                "111", R.drawable.img_news_home));
        mList.add(new NewsItemList("Sport", "Leonel Messi is leaving",
                "Leonel Messi is leaving Barcelona", "1 hours", "11",
                "90", R.drawable.img_business_interests));

        mAdapter.setItemList(mList);
        mAdapter.notifyDataSetChanged();

        mCategoryFirst.setText(mList.get(0).getCategory());
        mTitleFirst.setText(mList.get(0).getTitle());
        mTimeFirst.setText(mList.get(0).getTime());
        mCommentsNumber.setText(mList.get(0).getNumberOfComments());
        mSeenNumber.setText(mList.get(0).getNumberOfSeen());

        Glide.with(requireContext())
                .load(mList.get(0).getImgUrlNews())
                .into(mMainImg);
    }


    void startNewsInfo(NewsItemList newsItem) {
        Intent intent = new Intent(getContext(), NewsInfoActivity.class);
        intent.putExtra(Constants.NEWS_INFO, newsItem);
        startActivity(intent);
    }

    @Override
    public void onNewsItemClicked(NewsItemList itemList) {
        startNewsInfo(itemList);
    }
}