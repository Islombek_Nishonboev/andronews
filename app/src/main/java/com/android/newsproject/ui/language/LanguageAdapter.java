package com.android.newsproject.ui.language;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.newsproject.R;
import com.android.newsproject.data.model.LanguageItemList;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.ViewHolder> {

    List<LanguageItemList> mList = new ArrayList<>();


    public void setLanguageItemList(List<LanguageItemList> itemList) {
        mList = itemList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_language, parent, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.initView(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_flag_language)
        ImageView imgFlags;
        @BindView(R.id.name_country_language)
        TextView nameCountries;
        @BindView(R.id.btn_select_language)
        Button mBtnSelect;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }


        @RequiresApi(api = Build.VERSION_CODES.N)
        void initView(LanguageItemList itemList) {

            nameCountries.setText(itemList.getCountryName());

            Glide.with(itemView.getContext()).load(itemList.getImgFlags())
                    .transform(new RoundedCorners(11)).into(imgFlags);

            itemView.setOnClickListener(view -> {


                mList.forEach((n) -> n.setIsSelected(false));
                notifyDataSetChanged();


                itemList.setIsSelected(true);
                notifyDataSetChanged();
            });


            if (itemList.isSelected()) {
                mBtnSelect.setText("Selected");
                mBtnSelect.setBackgroundResource(R.drawable.bg_btn_select_language_selected);
                mBtnSelect.setTextColor(Color.parseColor("#FFFFFF"));
            } else {
                mBtnSelect.setBackgroundResource(R.drawable.bg_btn_select_language);
                mBtnSelect.setText("Select");
                mBtnSelect.setTextColor(Color.parseColor("#5C5C5C"));
            }
        }
    }
}

