package com.android.newsproject.ui.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.android.newsproject.R;

public class LoadingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_loading);


        new Handler().postDelayed(() -> {
            Intent mainIntent = new Intent(LoadingActivity.this, SplashActivity.class);
            LoadingActivity.this.startActivity(mainIntent);
            LoadingActivity.this.finish();
        }, 5000);
    }
}