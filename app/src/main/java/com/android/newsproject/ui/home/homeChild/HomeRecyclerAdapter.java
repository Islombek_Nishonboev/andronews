package com.android.newsproject.ui.home.homeChild;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.newsproject.R;
import com.android.newsproject.data.model.NewsItemList;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class HomeRecyclerAdapter extends RecyclerView.Adapter<HomeRecyclerAdapter.ViewHolder> {

    List<NewsItemList> mList = new ArrayList<>();
    private static OnNewsItemClickListener mListener;

    public void setListener(OnNewsItemClickListener listener) {
        mListener = listener;
    }


    public void setItemList(List<NewsItemList> itemList) {
        mList = itemList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_home, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.initView(mList.get(position + 1),position);
    }

    @Override
    public int getItemCount() {
        if (mList.isEmpty())
            return 0;
        return mList.size() - 1;
    }

    public interface OnNewsItemClickListener {
        void onNewsItemClicked(NewsItemList itemList);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.category_name)
        TextView mCategory;
        @BindView(R.id.title_news)
        TextView mTitle;
        @BindView(R.id.news_time)
        TextView mTime;
        @BindView(R.id.comments_number)
        TextView mComment;
        @BindView(R.id.seen_number)
        TextView mSeen;
        @BindView(R.id.img_news_item)
        ImageView mImg;

        NewsItemList mNewsItem;
        int position;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onNewsItemClicked(mNewsItem);
                }
            });
        }

        void initView(NewsItemList itemList,int bindPosition) {
            position = bindPosition;
            mNewsItem = itemList;
            mCategory.setText(itemList.getCategory());
            mTitle.setText(itemList.getTitle());
            mTime.setText(itemList.getTime());
            mComment.setText(itemList.getNumberOfComments());
            mSeen.setText(itemList.getNumberOfSeen());

            Glide.with(itemView.getContext()).load(itemList.getImgUrlNews()).into(mImg);
        }
    }
}
