package com.android.newsproject.ui.comments;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.newsproject.R;
import com.android.newsproject.data.constants.Constants;
import com.android.newsproject.data.model.NewsItemList;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LeaveCommentActivity extends AppCompatActivity {

    @BindView(R.id.edittext_leave)
    EditText editTextComment;
    @BindView(R.id.btn_leave)
    Button btnLeaveComment;
    @BindView(R.id.main_layout_comment)
    RelativeLayout mainLayout;
    @BindView(R.id.title_leave)
    TextView title;
    @BindView(R.id.category_leave)
    TextView category;
    @BindView(R.id.number_comments_leave)
    TextView numberComment;
    @BindView(R.id.img_news_leave)
    ImageView imgNews;

    NewsItemList newsItem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_leave_commment);
        ButterKnife.bind(this);

        //TextWatcher
        editTextComment.addTextChangedListener(textWatcher);

        newsItem = ((NewsItemList) getIntent().getSerializableExtra(Constants.LEAVE_COMMENT));
        title.setText(newsItem.getTitle());
        category.setText(newsItem.getCategory());
        numberComment.setText(String.valueOf(newsItem.getNumberOfComments()));
        Glide.with(this).load(newsItem.getImgUrlNews()).into(imgNews);


    }

    @OnClick(R.id.btn_back_leave)
    void onClickBack() {
        onBackPressed();
    }

    @OnClick({R.id.edittext_leave, R.id.main_layout_comment})
    void onClickEditText(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (imm != null)
            switch (view.getId()) {
                case R.id.edittext_leave: {
                    editTextComment.setFocusable(true);
                    editTextComment.setFocusableInTouchMode(true);
                    editTextComment.setBackground(getDrawable(R.drawable.bg_edittext_enabled));
                    imm.showSoftInput(editTextComment, InputMethodManager.SHOW_FORCED);

                    break;
                }
                case R.id.main_layout_comment: {
                    if (editTextComment.getText().toString().isEmpty()) {
                        editTextComment.setFocusable(false);
                        editTextComment.setBackground(getDrawable(R.drawable.bg_edittext_disabled));

                    }
                    imm.hideSoftInputFromWindow(editTextComment.getWindowToken(), 0);
                    break;
                }
            }
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            String text = editTextComment.getText().toString().trim();
            if (!text.isEmpty()) {
                btnLeaveComment.setBackground(getDrawable(R.drawable.bg_btn_enabled));
            } else {
                btnLeaveComment.setBackground(getDrawable(R.drawable.bg_btn_disabled));
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

}