package com.android.newsproject.ui.sign;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.newsproject.R;
import com.android.newsproject.data.constants.Constants;
import com.android.newsproject.ui.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SignInActivity extends AppCompatActivity {

    @BindView(R.id.username_sign_in)
    EditText mUserName;
    @BindView(R.id.password_sign_in)
    EditText mPassword;
    @BindView(R.id.btn_hide)
    ImageView btnHide;
    boolean hideAndShow = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

    }

    @OnClick({R.id.btn_login_sign_in, R.id.btn_hide, R.id.create_account_sign, R.id.forgot_password_sign_in,
            R.id.btn_skip_sign})
    void onClickSignIn(View view) {
        switch (view.getId()) {
            case R.id.btn_login_sign_in: {
                if (mUserName.getText().toString().isEmpty() || mPassword.getText().toString().isEmpty())
                    Toast.makeText(this, "Please fill Username and Password", Toast.LENGTH_SHORT).show();
                else{
                    Intent intent = new Intent(this,MainActivity.class);
                    intent.putExtra(Constants.IS_SIGN_IN,true);
                    startActivity(intent);
                }
                break;
            }

            case R.id.btn_hide: {
                if (hideAndShow) {
                    mPassword.setTransformationMethod(new PasswordTransformationMethod());
                    btnHide.setImageResource(R.drawable.ic_invisibility);
                    btnHide.setColorFilter(getColor(R.color.colorHideOffPassword));
                }
                if (!hideAndShow) {
                    mPassword.setTransformationMethod(null);
                    btnHide.setImageResource(R.drawable.ic_visibility);
                    btnHide.setColorFilter(getColor(R.color.colorHideOnPassword));
                }
                hideAndShow = !hideAndShow;
                break;
            }
            case R.id.create_account_sign: {
                startActivity(new Intent(this, SignUpActivity.class));
                break;
            }
            case R.id.forgot_password_sign_in: {
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                break;
            }
            case R.id.btn_skip_sign: {
                startActivity(new Intent(this, MainActivity.class));
                break;
            }
        }
    }
}