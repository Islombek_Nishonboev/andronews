package com.android.newsproject.ui.sign;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.newsproject.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPasswordActivity extends AppCompatActivity {

    @BindView(R.id.btn_send_forgot_password)
    Button btnSend;

    @BindView(R.id.email_forgot_password)
    EditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btn_send_forgot_password, R.id.btn_next_forgot_password})
    void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_send_forgot_password: {
                if (email.getText().toString().isEmpty())
                    Toast.makeText(this, "Type your email", Toast.LENGTH_SHORT).show();
                else startActivity(new Intent(this,SignInActivity.class));
                    break;
            }
            case R.id.btn_next_forgot_password:{
                startActivity(new Intent(this,SignInActivity.class));
            }
        }

    }
}