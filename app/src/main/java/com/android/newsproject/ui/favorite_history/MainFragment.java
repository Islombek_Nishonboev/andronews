package com.android.newsproject.ui.favorite_history;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.newsproject.R;
import com.android.newsproject.ui.MainActivity;
import com.android.newsproject.ui.callback.MainClickListener;
import com.android.newsproject.ui.favorite_history.history.HistoryRecyclerAdapter;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


@RequiresApi(api = Build.VERSION_CODES.R)
public class MainFragment extends Fragment {

    @BindView(R.id.tab_layout_favorite)
    TabLayout mTabLayout;
    @BindView(R.id.view_pager_favorite)
    ViewPager mViewPager;
    @BindView(R.id.delete_history)
    TextView mDelete;

    private List<String> mPageTitles = List.of("Favorites", "History");

    private MainPagerAdapter mPagerAdapter;

    private HistoryRecyclerAdapter mAdapter;

    MainClickListener mListener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mListener = ((MainActivity) context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main_favorite_history, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);


        //ViewPager
        mPagerAdapter = new MainPagerAdapter(getChildFragmentManager());
        mPagerAdapter.setPageTitles(mPageTitles);

        mViewPager.setAdapter(mPagerAdapter);

        mTabLayout.setupWithViewPager(mViewPager);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0: {
                        mDelete.setVisibility(View.INVISIBLE);
                        break;
                    }
                    case 1: {
                        mDelete.setVisibility(View.VISIBLE);
                        break;
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // History recyclerAdapter
        mAdapter = new HistoryRecyclerAdapter();
    }

    @OnClick({R.id.btn_menu_fv, R.id.btn_search_fv})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_menu_fv: {
                mListener.onNavBarClicked();
                break;
            }
            case R.id.btn_search_fv: {
                mListener.onSearchClicked();
                break;
            }
        }
    }
}