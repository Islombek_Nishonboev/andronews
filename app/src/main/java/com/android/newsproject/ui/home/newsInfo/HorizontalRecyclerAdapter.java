package com.android.newsproject.ui.home.newsInfo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.newsproject.R;
import com.android.newsproject.data.model.NewsItemList;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HorizontalRecyclerAdapter extends RecyclerView.Adapter<HorizontalRecyclerAdapter.ViewHolder> {

    List<NewsItemList> mList = new ArrayList<>();

    public void setItemList(List<NewsItemList> itemList){
        mList = itemList;
    }

    private static HorizontalRecyclerAdapter.OnOtherNewsItemClickListener mListener;

    public void setListener(HorizontalRecyclerAdapter.OnOtherNewsItemClickListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hr_recycler,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
       holder.initView(mList.get(position),position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public interface OnOtherNewsItemClickListener {
        void onOtherNewsItemClicked(NewsItemList itemList,int position);
    }

    class  ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_hr_recycler)
        ImageView image;
        @BindView(R.id.category_hr_recycler)
        TextView category;
        @BindView(R.id.title_hr_recycler)
        TextView title;
        NewsItemList newsItem;
        int newsPosition;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(view -> {
                mListener.onOtherNewsItemClicked(newsItem,newsPosition);
            });
        }
        void initView(NewsItemList itemList,int position){
            newsItem = itemList;
            newsPosition = position;
            Glide.with(itemView.getContext()).load(itemList.getImgUrlNews()).into(image);
            category.setText(itemList.getCategory());
            title.setText(itemList.getTitle());
        }
    }

}
