package com.android.newsproject.ui.comments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.newsproject.R;
import com.android.newsproject.data.constants.Constants;
import com.android.newsproject.data.model.CommentItem;
import com.android.newsproject.data.model.NewsItemList;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CommentsActivity extends AppCompatActivity {

    @BindView(R.id.recyclerview_comment)
    RecyclerView mRecycler;
    CommentsRecyclerAdapter mAdapter;

    @BindView(R.id.img_news_leave)
    ImageView mImg;
    @BindView(R.id.category_leave)
    TextView mCategory;
    @BindView(R.id.number_comments_leave)
    TextView mNumberComment;
    @BindView(R.id.title_leave)
    TextView mTitle;


    List<CommentItem> mCommentsList;

    NewsItemList newsItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_comments);
        ButterKnife.bind(this);

        //Recycler View
        mAdapter = new CommentsRecyclerAdapter();
        mRecycler.setLayoutManager(new LinearLayoutManager(this));
        mRecycler.setAdapter(mAdapter);
        mCommentsList = new ArrayList<>();
       addToComments();

       newsItem = ((NewsItemList) getIntent().getSerializableExtra(Constants.COMMENTS));
       mTitle.setText(newsItem.getTitle());
       mCategory.setText(newsItem.getCategory());
       mNumberComment.setText(String.valueOf(mCommentsList.size()));
        Glide.with(this).load(newsItem.getImgUrlNews()).into(mImg);



    }

    void addToComments() {
        mCommentsList.add(new CommentItem(R.drawable.img_default, "Nuke Treveller",
                "I really sad that happened", "1 hour"));
        mAdapter.setCommentList(mCommentsList);
        mAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.btn_comment)
    void onClick() {
      Intent intent =  new Intent(this, LeaveCommentActivity.class);
      intent.putExtra(Constants.LEAVE_COMMENT,newsItem);
      startActivity(intent);
    }

    @OnClick(R.id.btn_back_comment)
    void onBackClicked(){
        onBackPressed();
    }
}