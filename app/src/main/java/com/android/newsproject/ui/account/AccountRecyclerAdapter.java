package com.android.newsproject.ui.account;

import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.android.newsproject.R;
import com.android.newsproject.data.model.LeftNavigationItem;
import com.android.newsproject.ui.home.homeChild.HomeRecyclerAdapter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccountRecyclerAdapter extends RecyclerView.Adapter<AccountRecyclerAdapter.ViewHolder> {

    List<LeftNavigationItem> mList = new ArrayList<>();

    public void setItemList(List<LeftNavigationItem> list) {
        mList = list;
    }

    private static AccountRecyclerAdapter.onNavigationClicked mListener;

    public void setListener(AccountRecyclerAdapter.onNavigationClicked listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_navigation_left, parent, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.initView(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public interface onNavigationClicked{
        void onNavigationClicked(LeftNavigationItem itemList);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.indicator_left)
        View indicator;
        @BindView(R.id.img_navigation_left)
        ImageView mImgNavBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        void initView(LeftNavigationItem itemList) {


            Glide.with(itemView.getContext()).load(itemList.getImgUrl())
                    .into(mImgNavBar);


            itemView.setOnClickListener(view -> {
                mList.forEach((n) -> n.setSelected(false));
                notifyDataSetChanged();

                itemList.setSelected(true);
                notifyDataSetChanged();
                //

                mListener.onNavigationClicked(itemList);
            });

            if (itemList.isSelected()) {
                indicator.setVisibility(View.VISIBLE);
                mImgNavBar.setColorFilter(Color.parseColor("#FF1B1B"));
            } else {
                indicator.setVisibility(View.INVISIBLE);
                mImgNavBar.setColorFilter(Color.parseColor("#E8E8E8"));
            }
        }
    }
}
