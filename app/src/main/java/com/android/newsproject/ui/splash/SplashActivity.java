package com.android.newsproject.ui.splash;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.android.newsproject.R;
import com.android.newsproject.ui.language.LanguageActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.viewpager_splash)
    ViewPager mPager;

    SplashPagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);

        mPagerAdapter = new SplashPagerAdapter(getSupportFragmentManager());

        mPager.setAdapter(mPagerAdapter);

    }

    @OnClick(R.id.continue_splash)
    void onClickSplash() {
        if (mPager.getCurrentItem() != 0 && mPager.getCurrentItem() != 1)
            startActivity(new Intent(this, LanguageActivity.class));

        mPager.setCurrentItem(mPager.getCurrentItem() + 1, true);
    }
}