package com.android.newsproject.ui.sign;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.newsproject.R;
import com.android.newsproject.data.constants.Constants;
import com.android.newsproject.ui.MainActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends AppCompatActivity {

    @BindView(R.id.username_sign_up)
    EditText userName;
    @BindView(R.id.email_sign_up)
    EditText email;
    @BindView(R.id.password_sign_up)
    EditText password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_register_sign_up)
    void onRegisterClicked() {
        if (userName.getText().toString().isEmpty() || email.getText().toString().isEmpty()
                || password.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please fill blanks", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra(Constants.USER_NAME_SIGN_UP, userName.getText().toString());
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_skip)
    void onSkipClicked() {
        startActivity(new Intent(this, MainActivity.class));
    }
}