package com.android.newsproject.ui.interests;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.android.newsproject.R;
import com.android.newsproject.data.model.InterestsItemList;
import com.android.newsproject.ui.sign.SignInActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InterestsActivity extends AppCompatActivity {


    private InterestsRecyclerAdapter mAdapter;

    @BindView(R.id.recyclerview_interests)
    RecyclerView mRecycler;

    List<InterestsItemList> mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_interestes);
        ButterKnife.bind(this);

        mRecycler.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new InterestsRecyclerAdapter();
        mRecycler.setAdapter(mAdapter);

        mList = new ArrayList<>();

        addToInterests();

    }

    private void addToInterests() {
        mList.add(new InterestsItemList("World", R.drawable.img_world_interests));
        mList.add(new InterestsItemList("Business", R.drawable.img_business_interests));
        mList.add(new InterestsItemList("Politics", R.drawable.img_politics_interests));
        mList.add(new InterestsItemList("Technology", R.drawable.img_technology_interests));
        mList.add(new InterestsItemList("Travel", R.drawable.img_travel_interests));

        mAdapter.setInterestsItemList(mList);
        mAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.btn_continue_interests)
    void onClickInterests(){
        startActivity(new Intent(this, SignInActivity.class));
    }

}