package com.android.newsproject.ui.callback;

public interface MainClickListener {

    void onSearchClicked();

    void onHomeClicked();

    void onNavBarClicked();
}
