package com.android.newsproject.ui.language;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.android.newsproject.R;
import com.android.newsproject.data.model.LanguageItemList;
import com.android.newsproject.ui.interests.InterestsActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@RequiresApi(api = Build.VERSION_CODES.R)
public class LanguageActivity extends AppCompatActivity {


    List<LanguageItemList> mList;

    @BindView(R.id.recyclerview_language)
    RecyclerView mRecycler;

    private LanguageAdapter mAdapter;

    @BindView(R.id.btn_continue_language)
    Button btnContinue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_language);
        ButterKnife.bind(this);

        mRecycler.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new LanguageAdapter();
        mRecycler.setAdapter(mAdapter);

        mList = new ArrayList<>();

        addNewLanguage();

    }

    private void addNewLanguage() {
        mList.add(new LanguageItemList("Spain", R.drawable.ic_spain));
        mList.add(new LanguageItemList("France", R.drawable.ic_france));
        mList.add(new LanguageItemList("United Kingdom", R.drawable.ic_united_kingdom));
        mList.add(new LanguageItemList("Germany", R.drawable.ic_germany));
        mList.add(new LanguageItemList("United States", R.drawable.ic_usa));
        mAdapter.setLanguageItemList(mList);
        mAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.btn_continue_language)
    void onClickLanguage(View view) {
        startActivity(new Intent(this, InterestsActivity.class));
    }
}