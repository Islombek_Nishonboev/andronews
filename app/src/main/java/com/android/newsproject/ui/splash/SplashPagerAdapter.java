package com.android.newsproject.ui.splash;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

public class SplashPagerAdapter extends FragmentPagerAdapter {

    public SplashPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: {
                return new FirstSplashFragment();
            }
            case 1: {
                return new SecondSplashFragment();
            }
            case 2: {
                return new ThirdSplashFragment();
            }
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
