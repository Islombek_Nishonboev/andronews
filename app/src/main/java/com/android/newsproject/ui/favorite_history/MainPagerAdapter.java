package com.android.newsproject.ui.favorite_history;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.android.newsproject.ui.favorite_history.favorite.FavoriteFragment;
import com.android.newsproject.ui.favorite_history.history.HistoryFragment;
import com.android.newsproject.ui.splash.FirstSplashFragment;
import com.android.newsproject.ui.splash.SecondSplashFragment;

import java.util.List;

public class MainPagerAdapter extends FragmentPagerAdapter {

    List<String> mPageTitles;


    public void setPageTitles(List<String> pageTitles) {
        mPageTitles = pageTitles;
    }

    public MainPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }


    @RequiresApi(api = Build.VERSION_CODES.R)
    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: {
                return new FavoriteFragment();
            }
            case 1: {
                return new HistoryFragment();
            }
        }
        return null;
    }

    @Override
    public int getCount() {
        return mPageTitles.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mPageTitles.get(position);
    }


}
