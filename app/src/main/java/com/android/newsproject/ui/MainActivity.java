package com.android.newsproject.ui;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.newsproject.R;
import com.android.newsproject.data.constants.Constants;
import com.android.newsproject.ui.account.AccountFragment;
import com.android.newsproject.ui.callback.MainClickListener;
import com.android.newsproject.ui.category.CategoryActivity;
import com.android.newsproject.ui.favorite_history.MainFragment;
import com.android.newsproject.ui.home.HomeFragment;
import com.android.newsproject.ui.home.search.SearchFragment;
import com.android.newsproject.ui.interests.InterestsFragment;
import com.android.newsproject.ui.privacy.PrivacyActivity;
import com.android.newsproject.ui.sign.SignInActivity;
import com.android.newsproject.ui.splash.ThirdSplashFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, MainClickListener {

    @BindView(R.id.bottom_navigation_home)
    BottomNavigationView mBottomNavView;

    @BindView(R.id.drawerLayout)
    DrawerLayout mDrawer;

    @BindView(R.id.img_avatar)
    ImageView mImgAvatar;

    @BindView(R.id.name_avatar)
    TextView nameAvatar;
    @BindView(R.id.text_welcome)
    TextView textWelcome;
    @BindView(R.id.btn_sign)
    Button btnSign;

    Boolean isSignIn;


    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mBottomNavView.setOnNavigationItemSelectedListener(this);
        onPageChanged(new HomeFragment());

        //Img User Navigation Bar
        Glide.with(this).load(R.drawable.img_default)
                .transform(new RoundedCorners(360)).into(mImgAvatar);

//        Intent sign in
        isSignIn = getIntent().getBooleanExtra(Constants.IS_SIGN_IN, false);

        if (!isSignIn) {
//        Intent Sign Up
            String userName = getIntent().getStringExtra(Constants.USER_NAME_SIGN_UP);
            if (userName == null) {
                textWelcome.setText("Save Your Preferences");
                nameAvatar.setText("sign in to save your favorites");
                btnSign.setText("Sign in");
            } else nameAvatar.setText(userName);
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_home: {
                onPageChanged(new HomeFragment());
                break;
            }

            case R.id.menu_interests: {
                onPageChanged(new InterestsFragment());
                break;
            }

            case R.id.menu_favourites: {
                onPageChanged(new MainFragment());
                break;
            }

            case R.id.menu_privacy: {
                onPageChanged(new AccountFragment());
                break;
            }
        }
        return true;
    }

    private void onPageChanged(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_container, fragment)
                .commit();
    }

    @Override
    public void onSearchClicked() {
        onPageChanged(new SearchFragment());
    }

    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    public void onHomeClicked() {
        onPageChanged(new HomeFragment());
    }

    @Override
    public void onNavBarClicked() {
        mDrawer.openDrawer(GravityCompat.START);
    }

    @OnClick({R.id.category_navigation, R.id.privacy_navigation,R.id.btn_sign})
    void onClickNavBar(View view) {
        switch (view.getId()) {
            case R.id.category_navigation: {
                startActivity(new Intent(this, CategoryActivity.class));
                break;
            }
            case R.id.privacy_navigation: {
                startActivity(new Intent(this, PrivacyActivity.class));
                break;
            }
            case R.id.btn_sign:{
                startActivity(new Intent(this, SignInActivity.class));
                break;
            }
        }
    }

}