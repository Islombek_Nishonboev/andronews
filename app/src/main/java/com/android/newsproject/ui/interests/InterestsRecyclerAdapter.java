package com.android.newsproject.ui.interests;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.newsproject.R;
import com.android.newsproject.data.model.InterestsItemList;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InterestsRecyclerAdapter extends RecyclerView.Adapter<InterestsRecyclerAdapter.ViewHolder> {

    List<InterestsItemList> mList = new ArrayList<>();
    boolean flag = false;


    public void setInterestsItemList(List<InterestsItemList> itemList) {
        mList = itemList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_interests, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.initView(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_interests_item)
        ImageView mImageView;
        @BindView(R.id.text_interests_item)
        TextView mTextView;
        @BindView(R.id.btn_follow_interests)
        Button mBtnFollow;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void initView(InterestsItemList itemList) {
            mTextView.setText(itemList.getTitle());
            Glide.with(itemView.getContext()).load(itemList.getImgUrl()).into(mImageView);


            itemView.setOnClickListener(view -> {

                flag = !flag;

                itemList.setSelected(flag);
                notifyDataSetChanged();
            });

            if (itemList.isSelected()){
                mBtnFollow.setText("Followed");
                mBtnFollow.setTextColor(Color.parseColor("#FFFFFF"));
                mBtnFollow.setBackgroundResource(R.drawable.bg_btn_select_language_selected);
            }
            else {
                mBtnFollow.setText("+ Follow");
                mBtnFollow.setTextColor(Color.parseColor("#5C5C5C"));
                mBtnFollow.setBackgroundResource(R.drawable.bg_btn_select_language);
            }

        }
    }
}
