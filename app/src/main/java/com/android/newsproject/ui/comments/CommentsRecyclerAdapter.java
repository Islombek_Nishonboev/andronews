package com.android.newsproject.ui.comments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.newsproject.R;
import com.android.newsproject.data.model.CommentItem;
import com.android.newsproject.data.model.NewsItemList;
import com.android.newsproject.ui.home.homeChild.HomeRecyclerAdapter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentsRecyclerAdapter extends RecyclerView.Adapter<CommentsRecyclerAdapter.ViewHolder> {

    List<CommentItem> mList = new ArrayList<>();
    private static CommentsRecyclerAdapter.OnCommentsItemClickListener mListener;

    public void setCommentList(List<CommentItem> itemList) {
        mList = itemList;
    }


    public void setListener(CommentsRecyclerAdapter.OnCommentsItemClickListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comments, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.initView(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public interface OnCommentsItemClickListener {
        void onCommentsItemClicked(CommentItem itemList);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_avatar_comment)
        ImageView mImgAvatar;
        @BindView(R.id.name_avatar_comment)
        TextView mNameAvatar;
        @BindView(R.id.text_comment_item)
        TextView mTextComment;
        @BindView(R.id.time_comment)
        TextView mTimeComment;

        CommentItem commentItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(view -> mListener.onCommentsItemClicked(commentItem));
        }

        void initView(CommentItem item) {
            commentItem = item;
            mNameAvatar.setText(item.getNameAvatar());
            mTextComment.setText(item.getTextComment());
            mTimeComment.setText(item.getTimeComment());

            Glide.with(itemView.getContext())
                    .load(item.getImgUrl())
                    .transform(new RoundedCorners(25))
                    .into(mImgAvatar);
        }
    }
}
