package com.android.newsproject.ui.comments;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.newsproject.R;
import com.android.newsproject.data.constants.Constants;
import com.android.newsproject.data.model.CommentItem;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReplyCommentActivity extends AppCompatActivity {

    @BindView(R.id.img_avatar_reply)
    ImageView imgAvatar;
    @BindView(R.id.name_avatar_reply)
    TextView nameAvatar;
    @BindView(R.id.time_comment_reply)
    TextView timeComment;
    @BindView(R.id.text_comment_reply)
    TextView textComment;
    @BindView(R.id.edittext_reply)
    EditText editText;
    @BindView(R.id.btn_reply)
    Button btnReply;

    CommentItem commentItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_reply_comment);
        ButterKnife.bind(this);

        //getIntent
        commentItem = (CommentItem) getIntent().getSerializableExtra(Constants.REPLY_COMMENT);
        setting();

        editText.addTextChangedListener(textWatcher);

    }

    private void setting() {
        Glide.with(ReplyCommentActivity.this)
                .load(commentItem.getImgUrl())
                .transform(new RoundedCorners(360))
                .into(imgAvatar);
        nameAvatar.setText(commentItem.getNameAvatar());
        timeComment.setText(commentItem.getTimeComment());
        textComment.setText(commentItem.getTextComment());
    }

    @OnClick(R.id.btn_back_reply)
    void onClickBack(){
        onBackPressed();
    }

    @OnClick({R.id.edittext_reply, R.id.main_layout_reply})
    void onClickEditText(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (imm != null)
            switch (view.getId()) {
                case R.id.edittext_reply: {
                    editText.setFocusable(true);
                    editText.setFocusableInTouchMode(true);
                    editText.setBackground(getDrawable(R.drawable.bg_edittext_enabled));
                    imm.showSoftInput(editText, InputMethodManager.SHOW_FORCED);

                    break;
                }
                case R.id.main_layout_reply: {
                    if (editText.getText().toString().isEmpty()) {
                        editText.setFocusable(false);
                        editText.setBackground(getDrawable(R.drawable.bg_edittext_disabled));

                    }
                    imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                    break;
                }
            }
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            String text = editText.getText().toString().trim();
            if (!text.isEmpty()) {
                btnReply.setBackground(getDrawable(R.drawable.bg_btn_enabled));
            } else {
                btnReply.setBackground(getDrawable(R.drawable.bg_btn_disabled));
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };

}