package com.android.newsproject.ui.interests;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.newsproject.R;
import com.android.newsproject.data.model.InterestsItemList;
import com.android.newsproject.ui.MainActivity;
import com.android.newsproject.ui.callback.MainClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InterestsFragment extends Fragment {

    InterestsRecyclerAdapter mAdapter;


    @BindView(R.id.recyclerview_interests_fragment)
    RecyclerView mRecycler;

    List<InterestsItemList> mList;
    MainClickListener mListener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
       mListener = ((MainActivity) context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_interests, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        mAdapter = new InterestsRecyclerAdapter();
        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecycler.setAdapter(mAdapter);


        mList = new ArrayList<>();
        addToInterests();
    }

    private void addToInterests() {
        mList.add(new InterestsItemList("World", R.drawable.img_world_interests));
        mList.add(new InterestsItemList("Business", R.drawable.img_business_interests));
        mList.add(new InterestsItemList("Politics", R.drawable.img_politics_interests));
        mList.add(new InterestsItemList("Technology", R.drawable.img_technology_interests));
        mList.add(new InterestsItemList("Travel", R.drawable.img_travel_interests));

        mAdapter.setInterestsItemList(mList);
        mAdapter.notifyDataSetChanged();
    }
    @OnClick({R.id.btn_menu_interests,R.id.btn_search_interests})
    void onClick(View view){
        switch (view.getId()){
            case R.id.btn_menu_interests:{
                mListener.onNavBarClicked();
                break;
            }
            case R.id.btn_search_interests: {
                mListener.onSearchClicked();
                break;
            }
        }
    }
}