package com.android.newsproject.ui.home.newsInfo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.newsproject.R;
import com.android.newsproject.data.constants.Constants;
import com.android.newsproject.data.model.CommentItem;
import com.android.newsproject.data.model.NewsItemList;
import com.android.newsproject.ui.comments.CommentsActivity;
import com.android.newsproject.ui.comments.CommentsRecyclerAdapter;
import com.android.newsproject.ui.comments.LeaveCommentActivity;
import com.android.newsproject.ui.comments.ReplyCommentActivity;
import com.bumptech.glide.Glide;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewsInfoActivity extends AppCompatActivity implements HorizontalRecyclerAdapter.OnOtherNewsItemClickListener, CommentsRecyclerAdapter.OnCommentsItemClickListener {

    @BindView(R.id.recyclerview_news_info)
    RecyclerView mRecycler;
    @BindView(R.id.recyclerview_comments_info)
    RecyclerView mCommentsRecycler;
    @BindView(R.id.img_news_info)
    ImageView mImg;
    @BindView(R.id.title_news_info)
    TextView mTitle;
    @BindView(R.id.description_news_info)
    TextView mDescription;
    @BindView(R.id.seen_info)
    TextView mSeen;
    @BindView(R.id.time_info)
    TextView mTime;
    @BindView(R.id.number_comments_news_info)
    TextView numberComments;
    @BindView(R.id.view_favorite)
    View viewFavorite;

    HorizontalRecyclerAdapter mInfoAdapter;
    CommentsRecyclerAdapter mCommentAdapter;


    NewsItemList mItemList;
    List<CommentItem> mCommentsList;


    List<NewsItemList> mList;

    boolean favoriteSelector = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_news_info);
        ButterKnife.bind(this);

        mInfoAdapter = new HorizontalRecyclerAdapter();
        mRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mRecycler.setAdapter(mInfoAdapter);
        mInfoAdapter.setListener(this);

        //Comments
        mCommentAdapter = new CommentsRecyclerAdapter();
        mCommentsRecycler.setLayoutManager(new LinearLayoutManager(this));
        mCommentsRecycler.setAdapter(mCommentAdapter);
        mCommentsList = new ArrayList<>();
        mCommentAdapter.setListener(this);

        addToComments();

        //getIntent
        mItemList = (NewsItemList) getIntent().getSerializableExtra(Constants.NEWS_INFO);
        setNewsInfo();

        //Other news
        mList = new ArrayList<>();
        addToOtherNews();

        numberComments.setText(String.valueOf(mList.size()));


    }

    void addToComments() {
        mCommentsList.add(new CommentItem(R.drawable.img_default, "Nuke Treveller",
                "I really sad that happened", "1 hour"));
        mCommentAdapter.setCommentList(mCommentsList);
        mCommentAdapter.notifyDataSetChanged();
    }

    private void setNewsInfo() {
        Glide.with(this).load(mItemList.getImgUrlNews()).into(mImg);
        mTitle.setText(mItemList.getTitle());
        mDescription.setText(mItemList.getDescription());
        mTime.setText(mItemList.getTime());
        mSeen.setText(mItemList.getNumberOfSeen());
    }

    private void addToOtherNews() {
        mList.add(new NewsItemList("Sport", "Winter sports started", "Winter Sports started yesterday in Tokyo",
                "2 hour", "21", "32", R.drawable.img_travel_interests));
        mInfoAdapter.setItemList(mList);
        mInfoAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.btn_add_favourite)
    void onClickAddFavorite() {
        if (!favoriteSelector){
            viewFavorite.setVisibility(View.INVISIBLE);
        }
        else viewFavorite.setVisibility(View.VISIBLE);
        favoriteSelector = !favoriteSelector;
    }

    @OnClick({R.id.btn_back_news_info, R.id.btn_comments_news_info, R.id.btn_leave_comment})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back_news_info: {
                onBackPressed();
                break;
            }
            case R.id.btn_comments_news_info: {
                Intent intent = new Intent(this, CommentsActivity.class);
                intent.putExtra(Constants.COMMENTS, mItemList);
                startActivity(intent);
                break;
            }
            case R.id.btn_leave_comment: {
                Intent leaveIntent = new Intent(this, LeaveCommentActivity.class);
                leaveIntent.putExtra(Constants.LEAVE_COMMENT, mItemList);
                startActivity(leaveIntent);
                break;
            }
        }

    }

    void startNewsInfo(NewsItemList newsItem) {
        Intent intent = new Intent(this, NewsInfoActivity.class);
        intent.putExtra(Constants.NEWS_INFO, newsItem);
        startActivity(intent);
    }

    @Override
    public void onOtherNewsItemClicked(NewsItemList itemList, int position) {
        startNewsInfo(itemList);
        mList.remove(position);
        mInfoAdapter.setItemList(mList);
        mInfoAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCommentsItemClicked(CommentItem itemList) {
        startReplyComment(itemList);
    }

    void startReplyComment(CommentItem item) {
        Intent intentComment = new Intent(this, ReplyCommentActivity.class);
        intentComment.putExtra(Constants.REPLY_COMMENT, item);
        startActivity(intentComment);
    }
}