package com.android.newsproject.data.model;

import java.io.Serializable;

public class CommentItem implements Serializable {

    Integer imgUrl;
    String nameAvatar;
    String textComment;
    String timeComment;


    public CommentItem(Integer imgUrl, String nameAvatar, String textComment, String timeComment) {
        this.imgUrl = imgUrl;
        this.nameAvatar = nameAvatar;
        this.textComment = textComment;
        this.timeComment = timeComment;
    }

    public Integer getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(Integer imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getNameAvatar() {
        return nameAvatar;
    }

    public void setNameAvatar(String nameAvatar) {
        this.nameAvatar = nameAvatar;
    }

    public String getTextComment() {
        return textComment;
    }

    public void setTextComment(String textComment) {
        this.textComment = textComment;
    }

    public String getTimeComment() {
        return timeComment;
    }

    public void setTimeComment(String timeComment) {
        this.timeComment = timeComment;
    }

}
