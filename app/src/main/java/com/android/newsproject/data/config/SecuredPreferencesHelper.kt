package com.android.newsproject.data.config

import android.content.Context
import android.content.SharedPreferences
import android.util.Base64
import com.android.newsproject.di.AppContext

class SecuredPreferencesHelper(context: Context, prefsName: String) {
    var mSharedPreferences: SharedPreferences = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE)

    private fun encrypt(input: String): String {
        return Base64.encodeToString(input.toByteArray(), Base64.DEFAULT)
    }

    private fun decrypt(input: String?): String {
        return String(Base64.decode(input, Base64.DEFAULT))
    }

    fun put(key: String, value: Boolean) {
        var key = key
        key = encrypt(key)
        mSharedPreferences.edit().putBoolean(key, value).apply()
    }

    fun put(key: String, value: String) {
        encrypt(key)
        encrypt(value)
        mSharedPreferences.edit().putString(key, value).apply()
    }

    fun put(key: String, value: Int) {
        encrypt(key)
        mSharedPreferences.edit().putInt(key, value).apply()
    }

    fun put(key: String, value: Float) {
        encrypt(key)
        mSharedPreferences.edit().putFloat(key, value).apply()
    }

    fun put(key: String, value: Long) {
        encrypt(key)
        mSharedPreferences.edit().putLong(key, value).apply()
    }

    fun put(key: String, values: Set<String>) {
        encrypt(key)
        mSharedPreferences.edit().putStringSet(key, values).apply()
    }

    fun get(key: String, defaultValue: Boolean): Boolean {
        encrypt(key)
        return mSharedPreferences.getBoolean(key, defaultValue)
    }

    fun get(key: String, defaultValue: String): String {
        encrypt(key)
        encrypt(defaultValue)
        return decrypt(mSharedPreferences.getString(key, defaultValue))
    }

    fun get(key: String, defaultValue: Int): Int {
         encrypt(key)
        return mSharedPreferences.getInt(key,defaultValue)
    }

    fun get(key: String, defaultValue: Float): Float {
        encrypt(key)
        return mSharedPreferences.getFloat(key,defaultValue)
    }

    fun get(key: String, defaultValue: Long): Long {
        encrypt(key)
        return mSharedPreferences.getLong(key,defaultValue)
    }

    fun get(key: String, defaultValue: Set<String>): Set<String> {
        encrypt(key)
        return mSharedPreferences.getStringSet(key,defaultValue) as Set<String>
    }

    fun deleteSavedData(key: String){
        encrypt(key)
        mSharedPreferences.edit().remove(key).apply()
    }

    fun clearAllData(){
        mSharedPreferences.edit().clear().apply()
    }
}