package com.android.newsproject.data.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.android.newsproject.data.db.entity.SampleEntity;

@Database(entities = {SampleEntity.class}
        , version = 1)
public abstract class AppDatabase extends RoomDatabase {

}
