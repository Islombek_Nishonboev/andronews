package com.android.newsproject.data.model;

import android.content.Intent;

public class LanguageItemList {

    private String mCountryName;
    private Integer mImgFlags;
    private boolean mIsSelected = false;

    public boolean isSelected() {
        return mIsSelected;
    }

    public void setIsSelected(boolean mIsSelected) {
        this.mIsSelected = mIsSelected;
    }

    public LanguageItemList(String countryName, Integer imgFlags) {
        mCountryName = countryName;
        mImgFlags = imgFlags;
    }

    public String getCountryName() {
        return mCountryName;
    }

    public void setCountryName(String countryName) {
        mCountryName = countryName;
    }


    public Integer getImgFlags() {
        return mImgFlags;
    }

    public void setImgFlags(Integer imgFlags) {
        mImgFlags = imgFlags;
    }
}
