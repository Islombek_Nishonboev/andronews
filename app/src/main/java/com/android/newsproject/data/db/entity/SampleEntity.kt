package com.android.newsproject.data.db.entity

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import java.io.Serializable

@Entity(tableName = "sample")
class SampleEntity : Serializable {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    @Json(name = "id")
    lateinit var id:String
//
//    @ColumnInfo(name = "name")
//    @Json(name = "name")
//    var name: String? = null

}