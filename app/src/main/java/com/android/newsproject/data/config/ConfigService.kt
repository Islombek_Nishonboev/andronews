package com.android.newsproject.data.config

interface ConfigService {

    companion object {
        const val ACCESS_TOKEN = "ACCESS_TOKEN"
        const val LOCALE = "LOCALE"
    }

    fun getAccessToken(): String
    fun setAccessToken(token: String)
    fun getLocale(): String
    fun setLocale(locale: String)
}