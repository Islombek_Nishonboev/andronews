package com.android.newsproject.data.config

import android.content.Context

class ConfigServiceImpl( context: Context, configFileName: String) : ConfigService {

    var mPrefs: SecuredPreferencesHelper = SecuredPreferencesHelper(context, configFileName)

    override fun getAccessToken(): String {
        return mPrefs.get(ConfigService.ACCESS_TOKEN, "")
    }

    override fun setAccessToken(token: String) {
        mPrefs.put(ConfigService.ACCESS_TOKEN, token)
    }

    override fun getLocale(): String {
        return mPrefs.get(ConfigService.LOCALE, "")
    }

    override fun setLocale(locale: String) {
        mPrefs.put(ConfigService.LOCALE, locale)
    }

}