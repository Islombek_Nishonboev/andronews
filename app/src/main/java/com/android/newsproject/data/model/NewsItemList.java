package com.android.newsproject.data.model;

import java.io.Serializable;
import java.util.List;

public class NewsItemList implements Serializable {
    String category;
    String title;
    String description;
    String time;
    String numberOfComments;
    String numberOfSeen;
    Integer imgUrlNews;

//    List<CommentItem> mCommentItem;


    public NewsItemList(String category, String title, String description, String time,
                        String numberOfComments, String numberOfSeen, Integer imgUrlNews) {
        this.category = category;
        this.title = title;
        this.description = description;
        this.time = time;
        this.numberOfComments = numberOfComments;
        this.numberOfSeen = numberOfSeen;
        this.imgUrlNews = imgUrlNews;

//        mCommentItem = commentsItem;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNumberOfComments() {
        return numberOfComments;
    }

    public void setNumberOfComments(String numberOfComments) {
        this.numberOfComments = numberOfComments;
    }

    public String getNumberOfSeen() {
        return numberOfSeen;
    }

    public void setNumberOfSeen(String numberOfSeen) {
        this.numberOfSeen = numberOfSeen;
    }

    public Integer getImgUrlNews() {
        return imgUrlNews;
    }

    public void setImgUrlNews(Integer imgUrlNews) {
        this.imgUrlNews = imgUrlNews;
    }
}
