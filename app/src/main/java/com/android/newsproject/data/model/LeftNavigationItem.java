package com.android.newsproject.data.model;

public class LeftNavigationItem {
    Integer imgUrl;
    boolean isSelected;
    int orderNumber;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public LeftNavigationItem(Integer imgUrl,int orderNumber) {
        this.imgUrl = imgUrl;
        this.orderNumber = orderNumber;
    }

    public Integer getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(Integer imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }
}
