package com.android.newsproject.data.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.android.newsproject.data.config.ConfigService
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ApiServiceImpl(context: Context, api: Api) : ApiService {

    @Inject
    lateinit var mConfig: ConfigService

    private var mContext = context
    private var mApi = api
    lateinit var mAccessToken: String

    override fun setAuthKeys(accessToken: String) {
        mAccessToken = accessToken
    }

    override fun noConnection(): Boolean {
        val manager: ConnectivityManager = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (manager == null) return true

        val activeNetwork: NetworkInfo = manager.activeNetworkInfo ?: return true

        return !activeNetwork.isConnected

    }

//    fun getNewsCategories(locale: String?): Single<CategoriesResponse?>? {
//        return mApi.getNewsCategories(if (mLocale == "") RUSSIAN else locale)
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//    }
}