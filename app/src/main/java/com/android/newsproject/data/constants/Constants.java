package com.android.newsproject.data.constants;

public interface Constants {
    String NEWS_INFO = "NEWS_INFO";
    String COMMENTS = "COMMENTS";
    String USER_NAME_SIGN_UP = "USERNAMESIGNUP";
    String IS_SIGN_IN="IS_SIGN_IN";
    String REPLY_COMMENT="REPLY_COMMENT";
    String LEAVE_COMMENT="LEAVE_COMMENT";
}
