package com.android.newsproject.data.network

interface ApiService {

    fun setAuthKeys(accessToken: String)

    fun noConnection(): Boolean

}