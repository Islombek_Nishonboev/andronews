package com.android.newsproject.data.model;

import java.io.Serializable;

public class CategoryItem implements Serializable {
    private Integer imgUrl;
    private String categoryName;

    public CategoryItem(Integer imgUrl, String categoryName) {
        this.imgUrl = imgUrl;
        this.categoryName = categoryName;
    }

    public Integer getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(Integer imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
