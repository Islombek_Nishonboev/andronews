package com.android.newsproject.data.network.model

import com.squareup.moshi.Json

class SampleResponse {

    @Json(name = "status")
    var status = 0

    @Json(name = "message")
    var message: String? = null
}